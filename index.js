const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

//  chức năng thêm sinh viên
var dssv = [];
// lấy thông tin từ localStorage
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  //  array khi convert thành json sẽ mất function, ta sẽ map lại
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }

  renderDSSV(dssv);
}
function themSV() {
  var newSv = layThongTinTuForm();

  // var isValid = validator.kiemTraRong(newSV.ten, "spanMaSV");

  // kiểm tra mã sv
  var isValid =
    validator.kiemTraRong(
      newSv.ma,
      // .ma lấy trong file sv.model chỗ this.ma
      "spanMaSV",
      // spanMaSV là id bên html
      "Mã sinh viên không được để trống"
    ) &&
    validator.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên phải 4 kí tự",
      4,
      4
    );

  // kiểm tra tên sv
  isValid =
    isValid &
    validator.kiemTraRong(
      newSv.ten,
      "spanTenSV",
      "Tên sinh viên không được để trống"
    );

  // kiểm tra email
  isValid =
    isValid &
      validator.kiemTraRong(
        newSv.email,
        "spanEmailSV",
        "Email không được để trống"
      ) && validator.kiemTraEmail(newSv.email, "spanEmailSV", "Email bị sai");

  // kiểm tra mật khẩu sv
  isValid =
    isValid &
    validator.kiemTraRong(
      newSv.matKhau,
      "spanMatKhau",
      "Mật khẩu không được để trống"
    );
  // kiểm tra điểm toán
  isValid =
    isValid &
    validator.kiemTraRong(
      newSv.toan,
      "spanToan",
      "Điểm toán không được để trống"
    );

  // kiểm tra điểm lý
  isValid =
    isValid &
    validator.kiemTraRong(newSv.ly, "spanLy", "Điểm lý không được để trống");

  // kiểm tra điểm hóa
  isValid =
    isValid &
    validator.kiemTraRong(newSv.hoa, "spanHoa", "Điểm hóa không được để trống");

  if (isValid) {
    dssv.push(newSv);

    // tạo json
    var dssvJson = JSON.stringify(dssv);
    // lưu json vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);

    renderDSSV(dssv);
    1;
  }
}

function xoaSinhVien(id) {
  console.log(id);

  var index = timKiemViTri(id, dssv);
  // tìm thấy vị trí
  if (index != -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}

function capNhat() {
  var newSv = layThongTinTuForm();

  // kiểm tra mã sv
  var isValid =
    validator.kiemTraRong(
      newSv.ma,
      // .ma lấy trong file sv.model chỗ this.ma
      "spanMaSV",
      // spanMaSV là id bên html
      "Mã sinh viên không được để trống"
    ) &&
    validator.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên phải 4 kí tự",
      4,
      4
    );

  // kiểm tra tên sv
  isValid =
    isValid &
    validator.kiemTraRong(
      newSv.ten,
      "spanTenSV",
      "Tên sinh viên không được để trống"
    );

  // kiểm tra email
  isValid =
    isValid &
      validator.kiemTraRong(
        newSv.email,
        "spanEmailSV",
        "Email không được để trống"
      ) && validator.kiemTraEmail(newSv.email, "spanEmailSV", "Email bị sai");

  // kiểm tra mật khẩu sv
  isValid =
    isValid &
    validator.kiemTraRong(
      newSv.matKhau,
      "spanMatKhau",
      "Mật khẩu không được để trống"
    );
  // kiểm tra điểm toán
  isValid =
    isValid &
    validator.kiemTraRong(
      newSv.toan,
      "spanToan",
      "Điểm toán không được để trống"
    );

  // kiểm tra điểm lý
  isValid =
    isValid &
    validator.kiemTraRong(newSv.ly, "spanLy", "Điểm lý không được để trống");

  // kiểm tra điểm hóa
  isValid =
    isValid &
    validator.kiemTraRong(newSv.hoa, "spanHoa", "Điểm hóa không được để trống");

  if (isValid) {
    dssv.push(newSv);

    // tạo json
    var dssvJson = JSON.stringify(dssv);
    // lưu json vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);

    renderDSSV(dssv);
    1;
  }
}

function reSet() {
  var newSv = layThongTinTuForm();
  dssv.splice(newSv);
}

function search(ten_SV) {
  var index = timKiemViTri(ten_SV, dssv);
  if (index != -1) {
    renderDSSV(dssv);
  }
}
